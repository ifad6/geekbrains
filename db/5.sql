begin;

  -- Добавление сотрудника и его должности

  set @emp_no = 999;
  set @title = 'worker';

  insert into employees (emp_no) values (@emp_no);
  insert into titles (emp_no, title) values (@emp_no, @title);

commit;

-- Добавление начисления (salary) при приеме на работу, изменении должности, увольнении

-- Удаление департамента и данных о его менеджере, сотрудниках