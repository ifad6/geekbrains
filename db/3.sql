-- Страны и города мира
-- 1
select c.id, c.title city, r.title region, co.title country
from _cities c
       left join _regions r on r.id = c.region_id
       left join _countries co on co.id = c.country_id

-- 2
select c.*
from _cities c
       left join _regions r on r.id = c.region_id
where r.title = 'Московская область'


-- Сотрудники
-- 1
select d.dept_no, avg(s.salary) avg_salary
from dept_emp d
       left join salaries s using (emp_no)
group by d.dept_no

-- 2
select emp_no, max(salary) max_salary
from salaries
group by emp_no

-- 3
delete
from employees e
  left join dept_emp using (emp_no)
     left join titles using (emp_no)
     left join salaries using (emp_no)
where e.emp_no = (
      select emp_no
      from salaries
      order by salary desc
      limit 1
  )

-- 4
select dept_no, count(*) emp_count
from dept_emp
group by dept_no

-- 5
select de.dept_no, count(*) emp_count, sum(s.salary) sum_salary
from dept_emp de
       left join salaries s using (emp_no)
group by de.dept_no