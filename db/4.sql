-- 1

create algorithm=merge view city_info as
select c.id, c.title city, r.title region, co.title country
from _cities c
       left join _regions r on r.id = c.region_id
       left join _countries co on co.id = c.country_id

-- 2

delimiter $$

create function get_manager(fname varchar, lname varchar)
begin
  set result = (select * from employees where first_name like '%fname%' AND last_name like '%lname%' limit 1);
  return (result);
end;
$$

-- 3

create trigger add_employee after insert on employees for each row
begin
  insert into salaries (emp_no, salary, from_date, to_date) values (NEW.emp_no, 1000, now(), now())
end;