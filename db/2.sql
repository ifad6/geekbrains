ALTER TABLE `_countries`
  DROP `title_ru`, DROP `title_ua`, DROP `title_be`, DROP `title_en`, DROP `title_es`, DROP `title_pt`, DROP `title_de`, DROP `title_fr`, DROP `title_it`, DROP `title_pl`, DROP `title_ja`, DROP `title_lt`, DROP `title_lv`, DROP `title_cz`,
  CHANGE `country_id`	`id` INT NOT NULL PRIMARY KEY AUTO_INCERMENT,
  ADD `title` VARCHAR(150) NOT NULL INDEX;

ALTER TABLE `_regions`
  DROP `title_ru`, DROP `title_ua`, DROP `title_be`, DROP `title_en`, DROP `title_es`, DROP `title_pt`, DROP `title_de`, DROP `title_fr`, DROP `title_it`, DROP `title_pl`, DROP `title_ja`, DROP `title_lt`, DROP `title_lv`, DROP `title_cz`,
  CHANGE `region_id` `id` INT NOT NULL PRIMARY KEY AUTO_INCERMENT,
CHANGE `country_id` `country_id` INT NOT NULL FOREIGN KEY `_countries` (`id`),
ADD `title` VARCHAR(150) NOT NULL INDEX;

ALTER TABLE `_countries`
  DROP `title_ru`, DROP `title_ua`, DROP `title_be`, DROP `title_en`, DROP `title_es`, DROP `title_pt`, DROP `title_de`, DROP `title_fr`, DROP `title_it`, DROP `title_pl`, DROP `title_ja`, DROP `title_lt`, DROP `title_lv`, DROP `title_cz`,
  DROP `area_ru`, DROP `area_ua`, DROP `area_be`, DROP `area_en`, DROP `area_es`, DROP `area_pt`, DROP `area_de`, DROP `area_fr`, DROP `area_it`, DROP `area_pl`, DROP `area_ja`, DROP `area_lt`, DROP `area_lv`, DROP `area_cz`,
  DROP `region_ru`, DROP `region_ua`, DROP `region_be`, DROP `region_en`, DROP `region_es`, DROP `region_pt`, DROP `region_de`, DROP `region_fr`, DROP `region_it`, DROP `region_pl`, DROP `region_ja`, DROP `region_lt`, DROP `region_lv`, DROP `region_cz`,
  CHANGE `city_id`	`id` INT NOT NULL PRIMARY KEY AUTO_INCERMENT,
CHANGE `country_id` `country_id` INT NOT NULL FOREIGN KEY `_countries` (`id`),
CHANGE `important` `important` TINYINT(1) NOT NULL,
CHANGE `region_id` `region_id` INT NOT NULL FOREIGN KEY `_regions` (`id`),
ADD `title` VARCHAR(150) NOT NULL INDEX;