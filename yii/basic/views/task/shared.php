<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Task;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shared tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			//'id',
			'title',
			'description:ntext',
			//'creator_id',
			//'updater_id',
			'created_at',
			'updated_at',
			'users' => [
				'label' => 'Users',
				'content' => function (Task $model)
				{
					return implode(', ', $model->getSharedUsers()->select('username')->column());
				}
			],

			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update} {delete} {deleteAll}',
				'buttons' => [
					'deleteAll' => function ($url, Task $model, $key)
					{
						$icon = \yii\bootstrap\Html::icon('remove');
						return Html::a(
							$icon,
							['task-user/delete-all', 'taskId' => $model->id],
							[
								'data' => [
									'confirm' => 'Are you sure you want to delete this item?',
									'method' => 'post',
								]
							]
						);
					}
				]
			],
		],
	]); ?>
</div>
