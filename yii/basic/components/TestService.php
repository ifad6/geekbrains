<?php
namespace app\components;

use yii\base\Component;

class TestService extends Component
{
	public $headline = 'TestService::$headline';

	public function getHeadline()
	{
		return $this->headline;
	}
}