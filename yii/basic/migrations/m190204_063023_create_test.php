<?php

use yii\db\Migration;

/**
 * Class m190204_063023_create_test
 */
class m190204_063023_create_test extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable(
			'user',
			[
				'id' => $this->primaryKey(),
				'username' => $this->string()->notNull(),
				'password_hash' => $this->string()->notNull(),
				'auth_key' => $this->string()->null(),
				'creator_id' => $this->integer()->notNull(),
				'updater_id' => $this->integer()->null(),
				'created_at' => $this->integer()->notNull(),
				'updated_at' => $this->integer()->null(),
			]
		);

		$this->createTable(
			'task',
			[
				'id' => $this->primaryKey(),
				'title' => $this->string()->notNull(),
				'description' => $this->text()->notNull(),
				'creator_id' => $this->integer()->notNull(),
				'updater_id' => $this->integer()->null(),
				'created_at' => $this->integer()->notNull(),
				'updated_at' => $this->integer()->null(),
			]
		);

		$this->createTable(
			'task_user',
			[
				'id' => $this->primaryKey(),
				'task_id' => $this->integer()->notNull(),
				'user_id' => $this->integer()->notNull(),
			]
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('user');
		$this->dropTable('task');
		$this->dropTable('task_user');
	}

	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
			echo "m190204_063023_create_test cannot be reverted.\n";

			return false;
	}
	*/
}
