<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property string $auth_key
 * @property int $creator_id
 * @property int $updater_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Task[] $tasks
 * @property Task[] $tasks0
 * @property TaskUser[] $taskUsers
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
	public $password;

	public static function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return [
			[['username'], 'required'],
			[['creator_id', 'updater_id', 'created_at', 'updated_at'], 'integer'],
			[['username', 'password', 'auth_key'], 'string', 'max' => 255],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'username' => 'Username',
			'password_hash' => 'Password Hash',
			'auth_key' => 'Auth Key',
			'creator_id' => 'Creator ID',
			'updater_id' => 'Updater ID',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		];
	}

	public function behaviors()
	{
		return [
			TimestampBehavior::class,
			[
				'class' => BlameableBehavior::class,
				'createdByAttribute' => 'creator_id',
				'updatedByAttribute' => 'updater_id'
			]
		];
	}

	public function beforeSave($insert)
	{
		try
		{
			if (!parent::beforeSave($insert)) return false;

			if ($this->isNewRecord)
			{
				$this->auth_key = \Yii::$app->security->generateRandomString();
			}

			if ($this->password)
			{
				$this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
			}

			return true;
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	public function validatePassword($password)
	{
		return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
	}

	public static function findByUsername($name)
	{
		return User::findOne(['username' => $name]);
	}

	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findOne(['access_token' => $token]);
	}

	public function getId()
	{
		return $this->id;
	}

	public function getAuthKey()
	{
		return $this->auth_key;
	}

	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/* relations */

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccessedTasks()
	{
		return $this->hasMany(Task::className(), ['id' => 'task_id'])->via('taskUsers');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUpdaterTasks()
	{
		return $this->hasMany(Task::className(), ['updater_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreatorTasks()
	{
		return $this->hasMany(Task::className(), ['creator_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTaskUsers()
	{
		return $this->hasMany(TaskUser::className(), ['user_id' => 'id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \app\models\query\UserQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new \app\models\query\UserQuery(get_called_class());
	}
}
