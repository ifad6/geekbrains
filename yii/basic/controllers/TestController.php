<?php

namespace app\controllers;

use Yii;
use app\models\Product;
use yii\db\Expression;
use yii\web\Controller;
use \yii\db\Query;

class TestController extends Controller
{
	public function actionIndex()
	{
		$headline = Yii::$app->test->getHeadline();

		$product = new Product(['id' => 11, 'name' => 'Товар', 'created_at' => 4, 'price' => 5000]);

		return $this->render('index', [
			'headline' => $headline,
			'product' => $product,
		]);
	}

	public function actionInsert()
	{
		$arUsers = [
			[
				'username' => 'User 1',
				'password_hash' => md5(rand(0, 999)),
				'auth_key' => md5(rand(0, 999)),
				'creator_id' => 0,
				'updater_id' => NULL,
				'created_at' => time(),
				'updated_at' => NULL,
			],
			[
				'username' => 'User 2',
				'password_hash' => md5(rand(0, 999)),
				'auth_key' => md5(rand(0, 999)),
				'creator_id' => 0,
				'updater_id' => NULL,
				'created_at' => time(),
				'updated_at' => NULL,
			],
		];

		foreach ($arUsers as $user)
		{
			Yii::$app->db->createCommand()->insert('user', $user)->execute();
		}


		$arTasks = [
			[
				'title' => 'Task 1',
				'description' => 'Description',
				'creator_id' => 1,
				'updater_id' => NULL,
				'created_at' => time(),
				'updated_at' => NULL,
			],
			[
				'title' => 'Task 2',
				'description' => 'Description',
				'creator_id' => 2,
				'updater_id' => NULL,
				'created_at' => time(),
				'updated_at' => NULL,
			],
			[
				'title' => 'Task 3',
				'description' => 'Description',
				'creator_id' => 1,
				'updater_id' => NULL,
				'created_at' => time(),
				'updated_at' => NULL,
			],
		];

		Yii::$app->db->createCommand()->batchInsert('task', array_keys($arTasks[0]), $arTasks)->execute();
	}

	public function actionSelect()
	{
		$result = (new Query())->from('user')->where(['id' => 1])->one();
		_dump($result);

		$result = (new Query())->from('user')->where(['>', 'id', 1])->orderBy('username')->all();
		_dump($result);

		$result = (new Query())->from('user')->count();
		_dump($result);

		$result = (new Query())->from('task')->innerJoin('user', 'task.creator_id = user.id')->all();
		_dump($result);
	}
}
