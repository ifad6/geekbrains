<?php

namespace app\controllers;

use app\models\Task;
use Yii;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		/*if (Yii::$app->user->isGuest || Yii::$app->user->id != 1)
		{
			throw new ForbiddenHttpException();
		}*/

		$dataProvider = new ActiveDataProvider([
			'query' => User::find(),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single User model.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new User model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new User();

		if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			return $this->redirect(['view', 'id' => $model->id]);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			return $this->redirect(['view', 'id' => $model->id]);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = User::findOne($id)) !== null)
		{
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}

	public function actionTest()
	{
		// Создание пользователя
		$user = new User();
		$user->setAttributes([
			'username' => 'New User',
			'password_hash' => md5('test'),
			'creator_id' => 1,
			'created_at' => time(),
		]);
		$user->save();

		// Создание задач для пользователя
		$task = new Task();
		$task->setAttributes([
			'title' => 'New Task',
			'description' => 'Task task task',
			'created_at' => time(),
		]);
		$task->link('creator', $user);
		$task->save();

		$task = new Task();
		$task->setAttributes([
			'title' => 'New Task 2',
			'description' => 'Task task task 2',
			'created_at' => time(),
		]);
		$task->link('creator', $user);
		$task->save();

		$task = new Task();
		$task->setAttributes([
			'title' => 'New Task 3',
			'description' => 'Task task task 3',
			'created_at' => time(),
		]);
		$task->link('creator', $user);
		$task->save();

		// Чтение всех записей из User
		$arUsers = User::find()->with('creatorTasks')->all();
		//_dump($arUsers);

		// Чтение всех записей из User c JOIN
		$arUsers = User::find()->joinWith('creatorTasks')->all();
		//_dump($arUsers);

		$user = new User();
		$user->findOne(1);
		_dump($user->getAccessedTasks());
	}
}
