<?php
namespace console\controllers;

use yii\console\Controller;
use yii\console\ExitCode;

class HelloController extends Controller
{
	public function actionIndex()
	{
		$this->stdout("Hello World!\r\n");
		return ExitCode::OK;
	}
}
