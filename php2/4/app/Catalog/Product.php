<?php

namespace Catalog;

class Product
{
	public function getList(int $category, int $page = 1)
	{
		$offset = ($page - 1) * 10 - 1;
		return DB::array("SELECT * FROM `products` WHERE `category` = {$category} LIMIT 10 OFFSET {$offset}");
	}

	public function add(array $product)
	{
		$name = $product['name'];
		$price = (int)$product['price'];
		DB::query("INSERT INTO `products` (`name`, `price`) VALUES ('{$name}', {$price})");
	}

	public function generate()
	{
		$arNames = [
			['Красивый', 'Удобный', 'Надежный', 'Эргономичный', 'Премиум', 'Точный', 'Функциональный'],
			['белый', 'черный', 'красный', 'зеленый', 'оранжевый', 'желтый', 'синий'],
			['объектив', 'компьютер', 'автомобиль', 'прибор', 'телефон', 'нагреватель', 'стул']
		];

		return [
			'name' => $arNames[0][rand(0, 6)] . ' ' . $arNames[1][rand(0, 6)] . ' ' . $arNames[2][rand(0, 6)],
			'price' => rand (1, 100) * 100
		];
	}
}