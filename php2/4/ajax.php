<?php

require 'vendor/autoload.php';

use App\View\Page;
use Catalog\Product;

if (is_numeric($_GET['page'])) $page = $_GET['page'];
else $page = 1;

$product = new Product;
$arProducts = $product->getList((int)$_GET['category'], $page);

$page = new Page('productList');
echo $page->render(['arProducts' => $arProducts]);