<?php

/*
 * Класс обращения пользователя через форму
 * */

class Lead
{
	public $user;
	public $datetime;
	public $text;

	function add(int $user, string $text)
	{
		// Код добавления лида в базу
	}

	function get(int $id)
	{
		// Код чтения лида из базы
	}

	function update(array $data)
	{
		// Код обновления данных лида по ключу из массива
	}

	function delete(int $id)
	{
		// Код удаления лида из базы
	}
}