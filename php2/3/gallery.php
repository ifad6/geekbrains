<?php

require 'vendor/autoload.php';

use App\View\Page;


$arData = [];
$arPhotos = []; // массив с фото

if (is_numeric($_GET['img']))
{
	$photoId = (int)$_GET['img'];

	try
	{
		$photo = $arPhotos[ $photoId ];
		if (!$photo) throw new Exception('Invalid id');

		$template = 'gallery__simple';
		$arData['photo'] = $photo;
	}
	catch (Exception $e)
	{
		echo 'Error:' . $e->getMessage();
	}
}
else
{
	$template = 'gallery';
	$arData['arPhotos'] = $arPhotos;
}


$page = new Page($template);

echo $page->render($arData);