<?php

require 'vendor/autoload.php';

use App\View\Page;


try
{
	$dbh = new PDO('mysql:dbname=world;host=localhost', 'root', 'guessme');
}
catch (PDOException $e)
{
	echo "Error: Could not connect. " . $e->getMessage();
}

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

try
{
	$sql = "
		SELECT country.Code AS code, country.Name AS name, country.Region AS region, country.Population AS population,
			countrylanguage.Language AS language, city.Name AS capital
		FROM country, city, countrylanguage
		WHERE country.Code = city.CountryCode
			AND country.Capital = city.ID
			AND country.Code = countrylanguage.CountryCode
			AND countrylanguage.IsOfficial = 'T'
		ORDER BY population DESC LIMIT 0,20
	";
	$sth = $dbh->query($sql);
	while ($row = $sth->fetchObject())
	{
		$data[] = $row;
	}

	unset($dbh);


	$page = new Page('example6');

	echo $page->render([
		'data' => $data
	]);

}
catch (Exception $e)
{
	die ('ERROR: ' . $e->getMessage());
}