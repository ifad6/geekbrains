<?php

trait Singleton
{
	static private $singleton = null;

	private function __construct() { }

	private function __clone() { }

	static public function getInstance()
	{
		return (self::$singleton === null ? self::$singleton = new static() : self::$singleton);
	}
}

class Example
{
	use Singleton;
}