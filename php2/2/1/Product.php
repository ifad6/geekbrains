<?php

abstract class Product
{
	protected $basePrice = 5000;
	protected $profit;
	protected $price;

	public function calculatePrice()
	{
		$this->price = $this->basePrice;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function addProfit($quantity)
	{
		$this->profit += $this->price * $quantity;
	}
}