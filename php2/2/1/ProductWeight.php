<?php

class ProductWeight extends Product
{
	private $weight;

	public function setWeight(float $weight)
	{
		$this->weight = $weight;
	}

	public function calculatePrice()
	{
		$this->price = $this->basePrice * $this->weight;
	}
}