<?php

function power($val, $pow)
{
	return ($pow <= 0 ? 1 : $val * power($val, $pow - 1));
}