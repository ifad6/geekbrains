<?php

function getTime()
{
	$hour = date('G');
	$minute = date('i');

	if ($hour == 1 || $hour == 21) $srtHour = 'час';
	elseif (($hour >= 2 && $hour <= 4) || ($hour >= 22 && $hour <= 24)) $srtHour = 'часа';
	else $srtHour = 'часов';

	$lastMinute = $minute % 10;
	if ($minute >= 11 && $minute <= 14) $strMinute = 'минут';
	elseif ($lastMinute == 1) $strMinute = 'минута';
	elseif ($lastMinute >= 2 && $lastMinute <= 4) $strMinute = 'минуты';
	else $strMinute = 'минут';

	return "$hour $srtHour $minute $strMinute";
}

echo getTime();