<?php

require '3.php';

function mathOperation($arg1, $arg2, $operation)
{
	switch ($operation)
	{
		case 'sum':
		case 'diff':
		case 'div':
		case 'mult':
			return $operation($arg1, $arg2);
	}
}