<?php

$settlements = [
	'Московская область' => ['Москва', 'Зеленоград', 'Клин'],
	'Ленинградская область' => ['Санкт-Петербург', 'Всеволожск', 'Павловск', 'Кронштадт']
];

foreach ($settlements as $region => $cities)
{
	foreach ($cities as $k => $city)
	{
		if (!preg_match('#^к#iu', $city)) unset($cities[ $k ]);
	}

	echo $region . ':<br>' . implode(', ', $cities) . '<br>';
}