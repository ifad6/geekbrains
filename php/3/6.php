<?php

$arMenu = [
	'Главная',
	'Каталог',
	'Статьи' => ['О компании', 'Новости', 'Топ 10'],
	'Контакты',
];

function printMenu($arMenu)
{
	echo '<ul>';

	foreach ($arMenu as $key => $item)
	{
		echo '<li>';
		if (is_array($item))
		{
			echo $key;
			printMenu($item);
		}
		else echo $item;
		echo '</li>';
	}

	echo '</ul>';
}

printMenu($arMenu);