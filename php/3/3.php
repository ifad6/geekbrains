<?php

$settlements = [
	'Московская область' => ['Москва', 'Зеленоград', 'Клин'],
	'Ленинградская область' => ['Санкт-Петербург', 'Всеволожск', 'Павловск', 'Кронштадт']
];

foreach ($settlements as $region => $cities)
{
	echo $region . ':<br>' . implode(', ', $cities) . '<br>';
}