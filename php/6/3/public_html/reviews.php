<?php

/**
 * начальная страница сайта
 */

// поключаем конфигурации приложения
require '../config/main.php';
require '../engine/core.php';

// логика страницы
if (isset($_POST['add_review']))
{
	$text = strip_tags(htmlspecialchars($_POST['add_review']));
	execute("INSERT INTO `reviews` (`date`, `text`) VALUES (NOW(), {$text})");
}

$arReviews = getItemArray("SELECT * FROM `reviews` ORDER BY `date` DESC");

// вывод шаблона
echo render('site/example', [
	'arReviews' => $arReviews
]);