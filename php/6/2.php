<?php

if (isset($_GET['num1']) && isset($_GET['num2']) && isset($_GET['operation']))
{
	$num1 = (int)$_GET['num1'];
	$num2 = (int)$_GET['num2'];

	switch ($_GET['operation'])
	{
		case 'sum':
			$result = $num1 + $num2;
			break;
		case 'dif':
			$result = $num1 - $num2;
			break;
		case 'div':
			if (!$num2) $result = 'Ошибка — нельзя делить на ноль';
			else $result = $num1 / $num2;
			break;
		case 'mult':
			$result = $num1 * $num2;
			break;
		default:
			$result = 'Ошибка — неизвестная операция';
	}
}

?>

<!doctype html>
<html lang=ru>
<head>
	<meta charset="UTF-8">
	<title>Калькулятор</title>
</head>
<body>

<? if (isset($result)): ?>
	Результат:
	<div style="border: 2px solid #CCC; margin-bottom: 15px; padding: 5px 10px; width: 100px; display: inline-block;">
		<?= $result ?>
	</div>
<? endif ?>

<form>
	<table>
		<tr>
			<td>Число 1:</td>
			<td><input name="num1"></td>
		</tr>
		<tr>
			<td>Число 2:</td>
			<td><input name="num2"></td>
		</tr>
		<tr>
			<td>Операция:</td>
			<td>
				<button type="submit" name="operation" value="sum">+</button>
				<button type="submit" name="operation" value="dif">-</button>
				<button type="submit" name="operation" value="div">/</button>
				<button type="submit" name="operation" value="mult">*</button>
			</td>
		</tr>
	</table>
</form>

</body>
</html>
