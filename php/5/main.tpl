<h1>Галерея</h1>

<div class="gallery">
	<? foreach ($arPhotos as $photo): ?>
	<a href="?photo=<?= $photo['ID'] ?>" class="gallery__image">
		<img src="<?= $photo['URL'] ?>" alt=""><br>
		Просмотров: <?= $photo['COUNT'] ?>
	</a>
	<? endforeach ?>
</div>