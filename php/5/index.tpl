<!doctype html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Gallery — Main</title>
	<style>
		.gallery {
			margin-bottom: 30px;
		}

		.gallery__image {
			margin: 0 10px 10px 0;
			text-decoration: none;
		}

		.gallery__image img {
			max-width: 100px;
		}
	</style>
</head>
<body>

<? include $template . '.tpl' ?>

</body>
</html>