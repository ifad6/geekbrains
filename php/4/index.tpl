<!doctype html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Gallery</title>
	<style>
		.gallery {
			margin-bottom: 30px;
		}

		.gallery__image {
			margin: 0 10px 10px 0;
			text-decoration: none;
		}

		.gallery__image img {
			max-width: 100px;
		}
	</style>
</head>
<body>

<div class="gallery">
	<? foreach ($arImages as $image): ?>
		<a href="<?= $image ?>" class="gallery__image" target="_blank">
			<img src="<?= $image ?>" alt="">
		</a>
	<? endforeach ?>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.0.4/jquery.fancybox.pack.js"></script>
<script>
	$('.gallery__image').fancybox();
</script>

</body>
</html>