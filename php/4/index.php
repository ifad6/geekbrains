<?php

function getList($dir)
{
	$imgDir = './' . $dir;

	$arImages = [];
	$arFiles = scandir($imgDir);

	foreach ($arFiles as $file)
	{
		if (is_file($imgDir . $file)) $arImages[] = $dir . $file;
	}

	return $arImages;
}


$arImages = getList('img/');

include 'index.tpl';